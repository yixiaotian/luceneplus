package com.ld.zxw.init;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.store.FSDirectory;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.wltea.analyzer.lucene.IKAnalyzer;

import com.ld.zxw.config.Config;
import com.ld.zxw.config.DefaultObject;
@SuppressWarnings("all")
public class Init {


	Logger log = Logger.getLogger(Init.class);

	private Config config  = null;
	private Analyzer analyzer = null; 
	private String core;

	public Init(Config config,String core,Analyzer analyzer) {
		this.config = config;
		this.analyzer = analyzer;
		this.core = core;
	}
	public Init(Config config,String core) {
		this.config = config;
		this.analyzer = new IKAnalyzer(true);
		this.core = core;
	}

	/**
	 * 基础配置整合
	 * @throws IOException
	 */
	public Config start(){
		try {
			String lucene_core = config.getLucene_path()+this.core;
			config.setAnalyzer(analyzer);
			Path path=Paths.get(lucene_core+"/data");
			config.setDirectory(FSDirectory.open(path));
			config.setXmlDem(xmlObj(lucene_core+"/conf/config.xml"));
			String[] highlight_conf = config.getHighlight_conf();
			SimpleHTMLFormatter formatter = new SimpleHTMLFormatter(highlight_conf[0],highlight_conf[1]);
			config.setSimpleHTMLFormatter(formatter);
		} catch (Exception e) {
				e.printStackTrace();
		}
		return config;
	}

	public static List<DefaultObject> xmlObj(String xml_Url){
		List<DefaultObject> objs = new ArrayList<>();
		SAXReader saxReader = new SAXReader();
		try {
			Document read = saxReader.read(new File(xml_Url));
			List<Element> elements = read.getRootElement().elements();
			elements.forEach(e ->{
				DefaultObject defaultObject = new DefaultObject();
				defaultObject.setName(e.attributeValue("name"));
				defaultObject.setType(e.attributeValue("type"));
				String isQuery = e.attributeValue("isQuery");
				String isSort = e.attributeValue("isSort");
				String boost = e.attributeValue("boost");
				if(StringUtils.isNotBlank(isQuery)){
					isQuery = isQuery.toLowerCase();
					if(isQuery.equals("y")){
						defaultObject.setQuery(true);
					}else if(isQuery.equals("n")){
						defaultObject.setQuery(false);
					}
				}
				if(StringUtils.isNotBlank(isSort)){
					isSort = isSort.toLowerCase();
					//降序
					if(isSort.equals("desc")){
						defaultObject.setSort("true");
					}else if(isSort.equals("asc")){//升序
						defaultObject.setSort("false");
					}
				}
				if(StringUtils.isNotBlank(boost)){
					defaultObject.setBoost(Float.valueOf(boost));
				}
				objs.add(defaultObject);
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objs;

	}
}
