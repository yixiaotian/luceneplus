package com.ld.zxw.service;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.search.Query;

import com.ld.zxw.config.Config;
import com.ld.zxw.config.ConfigIndex;
import com.ld.zxw.config.IndexFactory;
import com.ld.zxw.config.LuceneDataSource;
import com.ld.zxw.index.AddIndex;
import com.ld.zxw.index.DeleteIndex;
import com.ld.zxw.index.QueryIndex;
import com.ld.zxw.index.UpdateIndex;
import com.ld.zxw.page.Page;
import com.ld.zxw.util.CommonUtil;
import com.ld.zxw.util.DateUtil;

public class LuceneServiceImpl implements LuceneService {
	
	private static String defaultKey = null;
	
	private static Config config = null;
	
	static{
		if(StringUtils.isNotBlank(defaultKey)){
			config = (Config) LuceneDataSource.dataSource.get(defaultKey);
		}else{
			config = (Config) LuceneDataSource.dataSource.get(LuceneDataSource.defaultKey);
		}
	}

	public LuceneServiceImpl(String defaultKeys) {
		defaultKey = defaultKeys;
	}

	public LuceneServiceImpl() {
		super();
	}

	@Override
	public boolean saveObjs(List<Object> objs) {
		long time = DateUtil.getDate();
		IndexFactory create = ConfigIndex.create(AddIndex :: new);
		boolean flag = create.saveIndexs(config, CommonUtil.ObjOrDoc(objs, config));
		DateUtil.timeConsuming("添加索引---》", time);
		return flag;
	}

	@Override
	public boolean saveMaps(@SuppressWarnings("rawtypes") List<HashMap> objs) {
		long time = DateUtil.getDate();
		IndexFactory create = ConfigIndex.create(AddIndex :: new);
		boolean flag = create.saveIndexs(config, CommonUtil.mapOrdoc(objs, config));
		DateUtil.timeConsuming("添加索引---》", time);
		return flag;
	}

	@Override
	public boolean delAll() {
		long time = DateUtil.getDate();
		IndexFactory create = ConfigIndex.create(DeleteIndex :: new);
		boolean flag = create.deleteAll(config);
		DateUtil.timeConsuming("删除索引---》", time);
		return flag;
	}

	@Override
	public boolean delKey(String field, String value) {
		long time = DateUtil.getDate();
		IndexFactory create = ConfigIndex.create(DeleteIndex :: new);
		boolean flag = create.deletekey(config, field, value);
		DateUtil.timeConsuming("删除索引---》", time);
		return flag;
	}

	@Override
	public boolean updateObjs(List<Object> objs,String field, String value) {
		long time = DateUtil.getDate();
		IndexFactory create = ConfigIndex.create(UpdateIndex :: new);
		boolean flag = create.updateIndexs(config, CommonUtil.ObjOrDoc(objs, config),field, value);
		DateUtil.timeConsuming("更新索引---》", time);
		return flag;
	}

	@Override
	public <T> List<T> findList(String value, Class<T> obj) {
		long time = DateUtil.getDate();
		IndexFactory create = ConfigIndex.create(QueryIndex :: new);
		List<T> findList = create.findList(config, value, obj);
		DateUtil.timeConsuming("查询索引---》", time);
		return findList;
	}

	@Override
	public <T> List<T> findRangeList(Query query, Class<T> obj) {
		long time = DateUtil.getDate();
		IndexFactory create = ConfigIndex.create(QueryIndex :: new);
		List<T> findList = create.findRangeList(config, query, obj);
		DateUtil.timeConsuming("查询索引---》", time);
		return findList;
	}

	@Override
	public <T> Page<T> findPageList(String value, int pageNumber, int pageSize, Class<T> obj) {
		long time = DateUtil.getDate();
		IndexFactory create = ConfigIndex.create(QueryIndex :: new);
		Page<T> findPageList = create.findPageList(config, value, pageNumber, pageSize, obj);
		DateUtil.timeConsuming("查询索引---》", time);
		return findPageList;
	}

	@Override
	public <T> Page<T> findRangePageList(Query query, int pageNumber, int pageSize, Class<T> obj) {
		long time = DateUtil.getDate();
		IndexFactory create = ConfigIndex.create(QueryIndex :: new);
		Page<T> findPageList = create.findRangePageList(config, query, pageNumber, pageSize, obj);
		DateUtil.timeConsuming("查询索引---》", time);
		return findPageList;
	}

	@Override
	public boolean deleteQuery(Query query) {
		long time = DateUtil.getDate();
		IndexFactory create = ConfigIndex.create(DeleteIndex :: new);
		boolean flag = create.deleteQuery(config, query);
		DateUtil.timeConsuming("删除索引---》", time);
		return flag;
	}
	
}
