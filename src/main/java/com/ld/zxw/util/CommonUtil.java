package com.ld.zxw.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ld.zxw.Documents.Documents;
import com.ld.zxw.config.Config;
import com.ld.zxw.config.DefaultObject;

public class CommonUtil {

	private static Logger log = Logger.getLogger(CommonUtil.class);

	public synchronized static IndexWriter getIndexWriter(Config config){
		IndexWriterConfig conf = new IndexWriterConfig(config.getAnalyzer());
		try {
			return new IndexWriter(config.getDirectory(), conf);
		} catch (IOException e) {
			log.error("获取 IndexWriter error:",e);
			return null;
		}
	}

	public synchronized static void colseIndexWriter(IndexWriter writer){
		if(writer != null){
			try {
				writer.close();
			} catch (IOException e) {
				log.error("关闭流异常:",e);
			}
		}
	}
	
	
	public synchronized static List<Document> ObjOrDoc(List<Object> objs,Config config){
		List<Document> documents = new ArrayList<>();
		List<DefaultObject> xmlDem = config.getXmlDem();
		int size = objs.size();
		for (int i = 0; i < size; i++) {
			JSONObject jsonObject = JSONObject.parseObject(JSON.toJSONString(objs.get(i)));
			Documents doc = new Documents();
			int size_ = xmlDem.size();
			for (int j = 0; j < size_; j++) {
				DefaultObject defaultObject = xmlDem.get(j);
				String name = defaultObject.getName();
				doc.put(name, jsonObject.get(name),defaultObject.getType(),defaultObject.getBoost());

			}
			documents.add(doc.getDocument());
		}
		return documents;
		
	}

	public synchronized static List<Document> mapOrdoc(@SuppressWarnings("rawtypes") List<HashMap> list,Config config){
		List<Document> documents = new ArrayList<>();
		List<DefaultObject> xmlDem = config.getXmlDem();
		int size = list.size();
		for (int i = 0; i < size; i++) {
			HashMap<?, ?> hashMap = list.get(i);
			Documents doc = new Documents();
			int size_ = xmlDem.size();
			for (int j = 0; j < size_; j++) {
				DefaultObject defaultObject = xmlDem.get(j);
				String name = defaultObject.getName();
				doc.put(name, hashMap.get(name),defaultObject.getType(),defaultObject.getBoost());

			}
			documents.add(doc.getDocument());
		}
		return documents;
	}

	/**
	 * 过滤非法字符
	 * @param str 元数据
	 * @param filter 非法数据
	 * @return
	 */
	public synchronized static String IllegalFiltering(String str,String filter){
		if(StringUtils.isEmpty(filter)){
			return str;
		}else{
			char[] data = getChar(str);
			char[] fiter = getChar(filter);
			String key="";
			for (int i = 0; i < data.length; i++) {
				if(isFlag(fiter, data[i])){
					key+=String.valueOf(data[i]);
				}
			}
			return key;
		}
	}
	private static char[] getChar(String str){
		char[] c=new char[str.length()];
		c=str.toCharArray();
		return c;
	}
	public static boolean isFlag(char[] fiter,char vue){
		boolean flag = true;
		for (int j = 0; j < fiter.length; j++) {
			if(fiter[j] == vue){
				flag=false;
				break;
			}
		}
		return flag;
	}

}
