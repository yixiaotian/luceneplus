package com.ld.zxw.config;

import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.search.Query;

import com.ld.zxw.page.Page;

public interface IndexFactory {

	//添加索引集合
	default boolean saveIndexs(Config config,List<Document> list){
		return false;
	};
	//添加索引对象
	default boolean saveIndex(Config config,Document document){
		return false;
	};
	//更新索引集合
	default boolean updateIndexs(Config config,List<Document> list,String field,String value){
		return false;
	};
	//更新索引对象
	default boolean updateIndex(Config config,Document document,String value){
		return false;
	};
	//清空索引
	default boolean deleteAll(Config config){
		return false;
	}
	//删除索引
	default boolean deletekey(Config config,String field,String value){
		return false;
	}
	//删除索引
	default boolean deleteQuery(Config config,Query query){
		return false;
	}

	default <T> Page<T> findPageList(Config config,String value,int pageNumber,int pageSize,Class<T> obj){
		return null;
	};
	default <T> Page<T> findRangePageList(Config config,Query query,int pageNumber,int pageSize,Class<T> obj){
		return null;
	};

	default <T> List<T> findList(Config config,String value,Class<T> obj){
		return null;
	}
	default <T> List<T> findRangeList(Config config,Query query,Class<T> obj){
		return null;
	}
}
