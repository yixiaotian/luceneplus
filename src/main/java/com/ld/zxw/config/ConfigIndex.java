package com.ld.zxw.config;

import java.util.function.Supplier;

public interface ConfigIndex {
	
	static IndexFactory create(Supplier <IndexFactory> supplier){
		return supplier.get();
	}

}
