package test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;

import com.ld.zxw.core.CoreFilter;
import com.ld.zxw.page.Page;
import com.ld.zxw.service.LuceneService;
import com.ld.zxw.service.LuceneServiceImpl;
@SuppressWarnings("all")
public class TestLucenePlus {
	
	static{
		CoreFilter filter = new CoreFilter();
		//自己的config 全路径
		filter.manualStart("test.DemoConfig");
	}
	
	private static Logger log = Logger.getLogger(TestLucenePlus.class);

	private static LuceneService service = new LuceneServiceImpl();




	public static void main(String[] args) throws Exception {
		//删除
		service.delAll();
		//添加
		boolean saveObjs = service.saveObjs(getDate());
		
		//分页查询
		Page<User> queryList = service.findPageList("络应", 20, 10, User.class);
		queryList.getList().forEach(e -> System.out.println(e));

		//更新
//		service.updateObjs(getuDate(),"一个");
		//查询 全部
//		List<User> queryList = service.findList("一个", User.class);
//		System.out.println(queryList.size());
//		for (int i = 0; i < queryList.size(); i++) {
//			System.out.println(queryList.get(i).getContent());
//			break;
//		}
//		System.out.println(queryList.size());
//		queryList.forEach(e -> System.out.println(e));
	}


	public static List<Object> getDate(){
		List<Object> list = new ArrayList<>();
		for (int i = 1; i < 200; i++) {
			User user = new User();
			user.setId(i);
			user.setName(null);
			user.setContent(i+"假设你正在创建一个社交网络应用。你想创建一个功能，使管理员能够执行任何的动作，如发送消息，对满足一定条件的社交网络应用的成员。下表详细描述了这种使用情况：");
			user.setSort(i);
			list.add(user);
		}
		return list;
	}
	public static List<Object> getuDate(){
		List<Object> list = new ArrayList<>();
		for (int i = 1; i < 200; i++) {
			User user = new User();
			user.setId(i);
			user.setName(null);
			user.setContent("222222222222222假设你正在创建一个社交网络应用。你想创建一个功能，使管理员能够执行任何的动作，如发送消息，对满足一定条件的社交网络应用的成员。下表详细描述了这种使用情况：");
			user.setSort(i);
			list.add(user);
		}
		return list;
	}
}
