package test;


import org.wltea.analyzer.lucene.IKAnalyzer;

import com.ld.zxw.config.Config;
import com.ld.zxw.config.LuceneDataSource;
import com.ld.zxw.core.LDConfig;
import com.ld.zxw.init.Init;
/**
 * config
 * @author zxw
 *
 */
public class DemoConfig extends LDConfig{
	
	/**
	 * 以下是基础配置  开启动态词库 优先使用本地词库
	 */
	@Override
	public void basicsConfig() {
		Config config = new Config();
		config.setLucene_path("d:/LuceneHome/core/");
		config.setQueryNum(Integer.MAX_VALUE);
		config.setHighlight(true);
		LuceneDataSource dataSource = new LuceneDataSource();
		dataSource.addDataSource(new Init(config, "test",new IKAnalyzer(false)).start());
		LuceneDataSource.DynamicDictionary=true;
		LuceneDataSource.redis_host = "127.0.0.1";
	}
	

}
