<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">
<meta name="renderer" content="webkit">
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="stylesheet" href="http://cdn.amazeui.org/amazeui/2.7.2/css/amazeui.css" />
<title>列表</title>
<style>
.header {
	text-align: center;
}

.header h1 {
	font-size: 200%;
	color: #333;
	margin-top: 30px;
}

.header p {
	font-size: 14px;
}
</style>
</head>
<body>
<div class="admin-content">
    <div class="admin-content-body">
      <div class="am-cf am-padding am-padding-bottom-0">
        <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg"><a href="${contextPath}/thesaurus">首页</a></strong> / <small>IK 动态词库</small></div>
      </div>

      <hr>

      <div class="am-g">
        <div class="am-u-sm-12 am-u-md-6">
          <div class="am-btn-toolbar">
            <div class="am-btn-group am-btn-group-xs">
              <a href="view/add.jsp"><button type="button" class="am-btn am-btn-default"><span class="am-icon-plus"></span>新增</button></a>
              <a href="${contextPath}/thesaurus?method=update"><button type="button" class="am-btn am-btn-default"><span class="am-icon-save"></span> 更新到正式索引库</button></a>
            </div>
          </div>
        </div>
        <div class="am-u-sm-12 am-u-md-3">
          <div class="am-form-group">
            <select data-am-selected="{btnSize: 'sm'}" style="display: none;" id="typeWord">
            	<option value="0">点击选择</option>
	              <c:if test="${typeWord == 'ext'}">
	              	<option value="ext" selected="selected">启用词</option>
	              </c:if>
	              <c:if test="${typeWord != 'ext'}">
	              	<option value="ext">启用词</option>
	              </c:if>
	              
	              <c:if test="${typeWord == 'stopword'}">
	              	<option value="stopword" selected="selected">停用词</option>
	              </c:if>
	              <c:if test="${typeWord != 'stopword'}">
	              	<option value="stopword">停用词</option>
	              </c:if>
            </select>
          </div>
        </div>
        <div class="am-u-sm-12 am-u-md-3">
          <div class="am-input-group am-input-group-sm">
            <input type="text" class="am-form-field" value="${searchName}" name="searchName" id="searchName">
          <span class="am-input-group-btn">
            <button class="am-btn am-btn-default" type="button" onclick="sendPage(1)">搜索</button>
          </span>
          </div>
        </div>
      </div>

      <div class="am-g">
        <div class="am-u-sm-12">
          <form class="am-form">
            <table class="am-table am-table-striped am-table-hover table-main">
              <thead>
              <tr>
                <th class="table-title">名称</th>
                <th class="table-type">类型</th>
                <th class="table-author">创建者</th>
                <th class="table-date">创建日期</th>
                <th class="table-set">操作</th>
              </tr>
              </thead>
              <tbody>
              <c:forEach var="row" items="${page.list}" varStatus="i">
	              <tr>
	                <td><a href="#">${row.name}</a></td>
	                <td>${row.type}</td>
	                <td class="am-hide-sm-only">${row.creator}</td>
	                <td class="am-hide-sm-only"><fmt:formatDate value="${row.createTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
	                <td>
	                  <div class="am-btn-toolbar">
	                    <div class="am-btn-group am-btn-group-xs">
	                        <button type="button" class="am-btn am-btn-default" onclick="window.location.href='${contextPath}/thesaurus?method=delete&index=${i.index}'"><span class="am-icon-trash-o"></span> 删除</button>
	                    </div>
	                  </div>
	                </td>
	              </tr>
              </c:forEach>
              </tbody>
            </table>
            <c:if test="${page.totalPage == 0}"> 没有数据了... </c:if>
            <c:if test="${page.totalPage != 0}">
	            <div class="am-cf">
	              		共 ${page.totalRow} 条记录 & 当前第${page.pageNumber}页
	              <div class="am-fr">
	                <ul class="am-pagination">
	                  <li onclick="sendPage(1)"><a href="#">首页</a></li>
	                  
	                  <c:if test="${page.pageNumber == 1}">
	                  	<li class="am-disabled"><a href="#">«</a></li>
	                  </c:if>
	                  <c:if test="${page.pageNumber != 1}">
	                  	<li onclick="sendPage(${page.pageNumber -1})"><a href="#">«</a></li>
	                  </c:if>
	                  
	                  <c:if test="${page.pageNumber == page.totalPage}">
	                  	<li class="am-disabled"><a href="#">»</a></li>
	                  </c:if>
	                  <c:if test="${page.pageNumber != page.totalPage}">
	                  	<li onclick="sendPage(${page.pageNumber +1})"><a href="#">»</a></li>
	                  </c:if>
	                  
	                  <li onclick="sendPage(${page.totalPage})"><a href="#">尾页</a></li>
	                </ul>
	              </div>
	            </div>
            </c:if>
            <hr>
            <p>LD.IK  索引库</p>
          </form>
        </div>

      </div>
    </div>
  </div>
</body>

<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="http://cdn.amazeui.org/amazeui/2.7.2/js/amazeui.js"></script>
<script src="http://cdn.amazeui.org/amazeui/2.7.2/js/amazeui.ie8polyfill.js"></script>
<script src="http://cdn.amazeui.org/amazeui/2.7.2/js/amazeui.widgets.helper.js"></script>
<script type="text/javascript">
function sendPage(pageNumber){
	var searchName = $("#searchName").val();
	var typeWord = $("#typeWord").val();
	if(searchName == undefined || searchName =='' || searchName == null){
		searchName ="";
	}
	if(typeWord == undefined || typeWord =='' || typeWord == null || typeWord == '0'){
		typeWord ="";
	}
	window.location.href="${contextPath}/thesaurus?method=list&pageNumber="+pageNumber+"&searchName="+searchName+"&typeWord="+typeWord; 
}
</script>
</html>